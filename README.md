bash | zsh command line-help
=================

### command line made cozy

Clone this repo to your home:
```
~/.dotfiles
```

If this is not your new director than you need to change bash/bashrc lines:
```
export DOTFILES=$HOME/.dotfiles
DOTFILES=$HOME/.dotfiles
```

or your zsh/zshrc similar lines depending on your choosing.

 ##Recommened install flow:
 1. Clone repo

    ` git clone --recursive https://github.com/decebal/dotfiles ~/.dotfiles `
 2. run install script

    ` cd ~/.dotfiles && ./install.sh `

### Inspiration:

- Github Article on dotfiles http://dotfiles.github.io/
- Zsh plugins: https://github.com/unixorn/awesome-zsh-plugins
